e3-vac_starting_switchyard  
======

ESS Site-specific EPICS module

This module controls if a Vacuum Pumping Group can pump down
*   from atmospheric pressure
*   from vacuum

It does this by comparing pressure from the two configured gauges. There were some problems during the LEBT commissioning and this was a proposed solution with the possibility that it would be needed for future sections.

It is unclear if this is needed anymore
